include("nn_eval.jl")

println("784, 5, 10")
net1 = train_nn(60000, 5, learning_const=5.0, n_layers=2, 
    num_neurons = [784, 5, 10])
test_nn(net1, 10000)

println("784, 30, 10")
s = time()
net2 = train_nn(60000, 5, learning_const=5.0, n_layers=2, 
    num_neurons = [784, 30, 10])
test_nn(net2, 10000)
println("elapsed time: ", time() - s)

println("784, 50, 10")
s = time()
net3 = train_nn(60000, 5, learning_const=5.0, n_layers=2, 
    num_neurons = [784, 50, 10])
test_nn(net3, 10000)
println("elapsed time: ", time() - s)

println("784, 100, 10")
s = time()
net4 = train_nn(60000, 5, learning_const=5.0, n_layers=2, 
    num_neurons = [784, 100, 10])
test_nn(net4, 10000)
println("elapsed time: ", time() - s)

println("784, 200, 10")
s = time()
net5 = train_nn(60000, 5, learning_const=5.0, n_layers=2, 
    num_neurons = [784, 200, 10])
test_nn(net5, 10000)
println("elapsed time: ", time() - s)

println("784, 400, 10")
s = time()
net6 = train_nn(60000, 5, learning_const=5.0, n_layers=2, 
    num_neurons = [784, 400, 10])
test_nn(net6, 10000)
println("elapsed time: ", time() - s)

println("784, 784, 10")
s = time()
net7 = train_nn(60000, 5, learning_const=5.0, n_layers=2, 
    num_neurons = [784, 784, 10])
test_nn(net7, 10000)
println("elapsed time: ", time() - s)
