# code: Julia implementation of backpropagation for neural nets? sigmoid neural nets?

LayerWeights = Array{Array{Float64, 2}, 1}

type NN
    weights::LayerWeights
    biases::Array{Array{Float64, 1}, 1}
end

# logistic function
function logistic(x)
    return 1./(1.+exp(-x))
end

function sigmoid_neuron{Float64}(input::Array{Float64}, weights::Array{Float64})
    x = dot(input, weights)
    return 1./(1.+exp(-x))
end

# Inputs:
#   weights - n1 x n0 matrix, where n0 is the length of the input and n1 is the number of neurons
#   inputs - n0 column vector
# Returns the outputs of the neurons.
function evaluate_layer(net::NN, inputs::Array{Float64, 1}, i::Int)
    x = net.weights[i]*inputs + net.biases[i]
    return 1 ./(1 .+exp(-x))
end

# Evaluates a multi-layer neural network.
function evaluate_nn(net::NN, inputs::Array{Float64, 1})
    old_inputs = inputs
    output = inputs
    for (index, layer) in enumerate(net.weights)
        old_inputs = output
        output = evaluate_layer(net, old_inputs, index)
    end
    return output
end

# Evaluates a multi-layer neural network, returning the results for every layer
function evaluate_nn_all(net::NN, inputs::Array{Float64})
    output = inputs
    outputs = Array(Array{Float64}, 0)
    push!(outputs, output)
    for (index, layer) in enumerate(net.weights)
        old_inputs = output
        output = evaluate_layer(net, old_inputs, index)
        push!(outputs, output)
    end
    return outputs
end

function sq_error(actual_out, expected_out)
    return sum((actual_out - expected_out).^2)
end

# TODO: implement biases?

# Updates the weights using backpropagation.
function update_weights(net::NN, inputs::Array{Float64, 1}, 
    correct_outputs::Array{Float64}; learning_const::Float64=0.01)
    layer_weights_new = Array(Array{Float64, 2}, length(net.weights))
    # 1. update the layer closest to the output
    last_layer = net.weights[end]
    actual_outs = evaluate_nn_all(net, inputs)
    actual_out = actual_outs[end]
    backward_pass = 2*((actual_out - correct_outputs).*(actual_out).*(1.-actual_out))
    last_layer_updates = backward_pass*(actual_outs[end-1]')
    #println(size(last_layer_updates))
    #println(last_layer_updates)
    #println(norm(last_layer_updates))
    #println(sq_error(actual_out, correct_outputs))
    #if (norm(last_layer_updates) > 10)
    #    last_layer_new = last_layer .+ (1/norm(last_layer_updates))*learning_const.*last_layer_updates;
    #    layer_weights_new[end] = last_layer_new
    #else
        last_layer_new = last_layer .- learning_const.*last_layer_updates;
        layer_weights_new[end] = last_layer_new
        net.biases[end] -= backward_pass
    #end
    # 2. work backwards, updating the layers from the output back to the input...
    # oh god no... what do I do...
    #actual_out = evaluate_nn(net, inputs)
    #println(sq_error(actual_out, correct_outputs))
    #println(sum(sum(last_layer_updates)));
    #println(norm(last_layer_updates));
    for (index, layer) in enumerate(reverse(net.weights[1:end-1]))
        # for each layer, and each unit, we sum the previous layer's stuff and...
        # use backward_pass
        index = length(net.weights) - index
        out = actual_outs[index+1]
        new_backward_pass = (net.weights[index+1]'*backward_pass).*(out.*(1-out))
        #new_backward_pass = new_backward_pass'
        #println(size(backward_pass))
        #println(size(net.weights[index+1]))
        #println(size(backward_pass'*net.weights[index+1]))
        #println(size(out))
        #println(size(new_backward_pass))
        #for i in 1:length(new_backward_pass)
        #    new_backward_pass[i] *= dot(layer_weights[index+1][:,i], backward_pass)
        #end
        updates = new_backward_pass*(actual_outs[index]')
        layer_weights_new[index] = layer .- learning_const.*updates
        net.biases[index] -= squeeze(new_backward_pass, 2)
        #println(sum(sum(updates)))
        backward_pass = new_backward_pass
    end
    net.weights = layer_weights_new
    return net
end
