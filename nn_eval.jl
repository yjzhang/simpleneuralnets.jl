# MNIST test code
using MNIST
include("nn.jl")

function train_nn(n_train::Int, iters::Int; learning_const=1.0, n_layers = 1,
    num_neurons = [784, 10])
    weights = Array(Array{Float64, 2}, 0)
    biases = Array(Array{Float64, 1}, 0)
    for i in 1:length(num_neurons)-1
        push!(weights, (0.5-rand(num_neurons[i+1], num_neurons[i]))./1000)
        push!(biases, 0.5-rand(num_neurons[i+1]))
    end
    net = NN(weights, biases) 
    accuracies_on_train = Array(Float64, 0)
    for k in 1:iters
        for i in shuffle([1:n_train])
            example = MNIST.trainfeatures(i)
            example = example - mean(example)
            example = example/norm(example)
            class = MNIST.trainlabel(i)
            #println("example ", i, ": ", class)
            solution = zeros(10)
            solution[int(class)+1] = 1.0
            old_solution = evaluate_nn(net, example)
            #println("old error: ", sum((old_solution-solution).^2))
            weights = update_weights(net, example, solution, learning_const=learning_const)
            new_solution = evaluate_nn(net, example)
            #println("new error: ", sum((new_solution-solution).^2))
        end
        println("iter ", k, " done")
        acc = test_nn_on_train(net, n_train)
        push!(accuracies_on_train, acc)
    end
    return (net, accuracies_on_train)
end

function test_nn(net::NN, n_test::Int)
    n_correct = 0
    for i in 1:n_test
        example = MNIST.testfeatures(i)
        example = example - mean(example)
        example = example/norm(example)
        class = MNIST.testlabel(i)
        solution = zeros(10)
        solution[int(class)+1] = 1.0
        actual_solution = evaluate_nn(net, example)
        #println("actual class: ", class)
        #println(actual_solution)
        #println("guessed class: ", indmax(actual_solution) - 1)
        if int(class) == indmax(actual_solution) - 1
            n_correct+=1
        end
        #println()
    end
    println(n_correct, " correct out of ", n_test)
    return n_correct/n_test
end

function test_nn_on_train(net::NN, n_test::Int)
    n_correct = 0
    for i in 1:n_test
        example = MNIST.trainfeatures(i)
        example = example - mean(example)
        example = example/norm(example)
        class = MNIST.trainlabel(i)
        solution = zeros(10)
        solution[int(class)+1] = 1.0
        actual_solution = evaluate_nn(net, example)
        #println("actual class: ", class)
        #println(actual_solution)
        #println("guessed class: ", indmax(actual_solution) - 1)
        if int(class) == indmax(actual_solution) - 1
            n_correct+=1
        end
        #println()
    end
    println(n_correct, " correct out of ", n_test)
    return n_correct/n_test
end
