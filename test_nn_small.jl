include("nn_eval.jl")

println("784, 5, 10")
(net1, acc1) = train_nn(100, 100, learning_const=1.0, n_layers=2, 
    num_neurons = [784, 5, 10])
test_nn(net1, 100)

println("784, 30, 10")
s = time()
(net2, acc2) = train_nn(60000, 5, learning_const=1.0, n_layers=2, 
    num_neurons = [784, 30, 10])
test_nn(net2, 100)
println("elapsed time: ", time() - s)

println("784, 50, 10")
s = time()
(net3, acc3) = train_nn(100, 100, learning_const=1.0, n_layers=2, 
    num_neurons = [784, 50, 10])
test_nn(net3, 100)
println("elapsed time: ", time() - s)

println("784, 100, 10")
s = time()
(net4, acc4) = train_nn(100, 100, learning_const=1.0, n_layers=2, 
    num_neurons = [784, 100, 10])
test_nn(net4, 100)
println("elapsed time: ", time() - s)
